package com.ryan.demo.model;

import javax.persistence.*;

//represents a table in a relational database
@Entity

//display data in tabular form
@Table(name="categories")
public class Demo {
	//primary key
	@Id
	//auto increment
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	
	//constructor
	public Demo() {
		
	}
	public Demo(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	//encap
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "Demo [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
	
}
