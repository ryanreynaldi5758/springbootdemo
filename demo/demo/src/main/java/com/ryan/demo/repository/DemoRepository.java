package com.ryan.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ryan.demo.model.Demo;
/*
 *     T: Domain type that repository manages 
 *     (Generally the Entity/Model class name)
 *     
 *     ID: Type of the id of the entity that 
 *     repository manages (Generally the wrapper 
 *     class of your @Id that is created inside 
 *     the Entity/Model class)
 */
@Repository
public interface DemoRepository extends JpaRepository<Demo, Long> {
	//custom finder

}