package com.ryan.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ryan.demo.model.Demo;
import com.ryan.demo.repository.DemoRepository;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class DemoController {
	@Autowired
	DemoRepository demoRepository;

	// getmapping("/nama tabel db)
	@GetMapping("/category")
	public List<Demo> getAllCategory() {
		return demoRepository.findAll();
	}
}
